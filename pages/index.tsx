import Image from "next/image";
import { Inter } from "next/font/google";
import { useEffect, useState } from "react";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const [taskName, setTaskName] = useState<string | undefined>();
  const [task, setTask] = useState<any[]>([]);

  const handleAddTodo = (name: string) => {
    setTask((prev) => [
      ...prev,
      { name, complete: false, id: task.length + 1 },
    ]);
  };

  const handleComplete = (id: number, val: boolean) => {
    let updateTask:any = task.filter((el:any) => el.id === id).pop()
    
    let data = {...updateTask, complete: val};

    setTask([...task.filter((el:any) => el.id !== id), data])
  };
  
  const handleDelete = (id:number) => {
    setTask(task.filter((el:any) => el.id !== id))
  }

  return (
    <div className="m-auto w-full max-w-4xl bg-gray-200 min-h-[100vh] p-12 space-y-12">
      <div className="flex gap-2">
        <input
          type="text"
          className="p-2 rounded-md w-4/5 "
          value={taskName}
          onChange={(e: any) => setTaskName(e.target.value)}
        />
        <button
          className="rounded-md p-2 bg-blue-400 w-full max-w-[200px]"
          onClick={() => handleAddTodo(taskName!)}
        >
          Save
        </button>
      </div>

      <div>
        {task.map((el: any, i: number) => {
          return (
            <div key={i} className="flex gap-4 items-center">
              <div className={`${el.complete ? "line-through" : ""}`}>
                {el.name}
              </div>
              <input
                type="checkbox"
                defaultValue={el.complete}
                onChange={(e: any) => handleComplete(el.id, e.target.checked)}
              />
              <button className="p-2 rounded-lg bg-red-200" onClick={() => handleDelete(el.id)}>delete</button>
            </div>
          );
        })}
      </div>
    </div>
  );
}
